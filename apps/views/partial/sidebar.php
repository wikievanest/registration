<!-- APP ASIDE ==========-->
<aside id="menubar" class="menubar light">
  <div class="app-user">
    <div class="media">
      <div class="media-left">
        <div class="avatar avatar-md avatar-circle">
          <a href="javascript:void(0)"><i class="zmdi zmdi-account-circle zmdi-hc-4x"></i></a>
        </div><!-- .avatar -->
      </div>
      <div class="media-body">
        <div class="foldable">
          <h5><a href="javascript:void(0)" class="username"><?php echo $this->session->userdata('first_name').' '.$this->session->userdata('last_name');?></a></h5>
          <ul>
            <li class="dropdown">
              <a href="javascript:void(0)" class="dropdown-toggle usertitle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              </a>
            </li>
          </ul>
        </div>
      </div><!-- .media-body -->
    </div><!-- .media -->
  </div><!-- .app-user -->

  <div class="menubar-scroll">
    <div class="menubar-scroll-inner">
      <ul class="app-menu">
       <!-- Member Access -->
        <?php
          if($this->session->userdata('level')==1 || $this->session->userdata('level')==5) {
        ?>

        <li>
          <a href="<?=base_url('home');?>">
            <i class="menu-icon zmdi zmdi-view-dashboard zmdi-hc-lg"></i>
            <span class="menu-text">Home</span>
          </a>
        </li>

        <li class="has-submenu">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon zmdi zmdi-account zmdi-hc-lg"></i>
            <span class="menu-text">Patients</span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
          <ul class="submenu">
            <li><a href="<?=base_url('searchpatient');?>"><span class="menu-text">Search Patient</span></a></li>
            <!-- <li><a href="<?=base_url('patients');?>"><span class="menu-text">Patient List</span></a></li> -->
          </ul>
        </li>
<!--         <li class="has-submenu">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon zmdi zmdi-inbox zmdi-hc-lg"></i>
            <span class="menu-text">Encounters</span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
          <ul class="submenu">
            <li><a href="<?=base_url('encounters');?>"><span class="menu-text">Encounter list</span></a></li>
          </ul>
        </li> -->

        <li class="has-submenu">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon zmdi zmdi-chart zmdi-hc-lg"></i>
            <span class="menu-text">Reports</span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
          <ul class="submenu">
            <li><a href="<?=base_url('patientbydate');?>"><span class="menu-text">Patient by Date</span></a></li>
            <li><a href="<?=base_url('patientbystate');?>"><span class="menu-text">Patient by State</span></a></li>
            <li><a href="<?=base_url('patientbyencounter');?>"><span class="menu-text">Patient by Encounter</span></a></li>
            <!-- <li><a href="<?=base_url('doctors');?>"><span class="menu-text">Providers</span></a></li> -->
          </ul>
        </li>
        
        <?php } ?>
        
        <?php
          if($this->session->userdata('level')==0) {
        ?>
        <li>
          <a href="javascript:void(0)">
            <i class="menu-icon zmdi zmdi-settings zmdi-hc-lg"></i>
            <span class="menu-text">Users</span>
          </a>
        </li>
        <?php } ?>
        <hr>
        
        <?php
          if($this->session->userdata('level')==0) {
        ?>
        <footer id="aside-footer">
        <ul class="aside-menu aside-left-menu">
          <li class="menu-item">
            <a href="javascript:void(0)" class="menu-link">
              <span class="menu-icon"><i class="zmdi zmdi-settings zmdi-hc-lg"></i></span>
              <span class="menu-text foldable">Settings</span>
            </a>
          </li><!-- .menu-item -->
        </ul>
      </footer><!-- #sidebar-footer -->
        <?php } ?>
      </ul><!-- .app-menu -->
    </div><!-- .menubar-scroll-inner -->
  </div><!-- .menubar-scroll -->
</aside>