<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Admin, Dashboard, Bootstrap" />
  <link rel="shortcut icon" sizes="196x196" href="">
  <title><?php echo $template['title']; ?></title>

  <link rel="stylesheet" href="<?= base_url('public/assets/css/style.css'); ?>">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>


<body class="bg-light" style="font-family: sans-serif;">
  <nav style="float:right;margin: 10px">
  <label><?php echo $this->lang->line('language'); ?></label>
  <select class="form-control" onchange="javascript:window.location.href='<?php echo base_url('home/change_language'); ?>/'+this.value;">
    <option value="english" <?php if ($this->session->userdata('site_lang') == 'english') echo 'selected="selected"'; ?>>English</option>
    <option value="spanish" <?php if ($this->session->userdata('site_lang') == 'spanish') echo 'selected="selected"'; ?>>Spanish</option>
  </select>
  </nav>
  <div class="container">
    <!--========== END app aside -->
    <!-- APP MAIN ==========-->
    <div class="container">
      <?php echo $template['body']; ?>
    </div><!-- .wrap -->


    <script src="<?= base_url('public/assets/js/form-validation.js'); ?>"></script>
    <!-- endbuild -->
</body>

</html>