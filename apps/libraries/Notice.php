<?php
/**
 * @FileInfo: /Users/wikie/Development/Sites/cimco/apps/libraries/Notice.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-07-20 12:09:49
 * @Last Modified by:   wikie
 * @Last Modified time: 2016-07-28 20:58:49
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class notice {

        public function get_message($status, $message) {
		$CI =& get_instance();
		$message = array(
				'message' => $message,
				'class' => $status);
		$CI->session->set_flashdata('notification', $message);
		// $CI->session->keep_flashdata('notification');
	
	}
}