<?php
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmapp/apps/core/MY_Controller.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-07-26 09:09:11
 * @Last Modified by:   wikie
 * @Last Modified time: 2016-07-26 09:26:09
 */

class MY_Controller extends CI_Controller{   
    public function __construct()
    {
        parent::__construct();                
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
    } 
}
