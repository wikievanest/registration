<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

# HEADER 
$lang['language']      = 'seleccione el idioma';
$lang['header']     = 'CUESTIONARIO DE HISTORIAL DE SALUD';
$lang['desc']       = 'Todas las preguntas contenidas en este cuestionario son estrictamente confidenciales y pasarán a formar parte de su historial médico';

# LEGEND
$lang['leg_personal']      = 'Informaciones personales';
$lang['leg_health_history']      = 'HISTORIAL DE SALUD PERSONAL';
$lang['leg_health_habbit']      = 'HÁBITOS DE SALUD Y SEGURIDAD PERSONAL';
$lang['leg_health_habbit_desc']       = 'TODAS LAS PREGUNTAS CONTENIDAS EN ESTE CUESTIONARIO SON OPCIONALES Y SE GUARDARÁN ESTRICTAMENTE CONFIDENCIALES.';
$lang['leg_family']      = 'ANTECEDENTES DE SALUD FAMILIAR';
$lang['leg_mental_health']      = 'SALUD MENTAL';
$lang['leg_women_only']      = 'SOLO PARA MUJERES';
$lang['leg_men_only']      = 'SÓLO PARA HOMBRES';
$lang['leg_other_problem']      = 'OTROS PROBLEMAS';

# SECTION

$lang['sec_exercise']      = 'Ejercicio';
$lang['sec_diet']      = 'Dieta';
$lang['sec_caffeine']      = 'Cafeína';
$lang['sec_alcohol']      = 'Alcohol';
$lang['sec_tobacco']      = 'Tabaco';
$lang['sec_drugs']      = 'Drogas';
$lang['sec_sex']      = 'Sexo';
$lang['sec_personal_safety']      = 'Seguridad personal';
$lang['sec_father']      = 'Padre';
$lang['sec_mother']      = 'Madre';
$lang['sec_sibling']      = 'Hermano';
$lang['sec_children']      = 'Niños';
$lang['sec_grandmother_mat']      = 'Abuela Materna';
$lang['sec_grandfather_mat']      = 'Abuelo Materno';
$lang['sec_grandmother_pat']      = 'Abuela Paterna';
$lang['sec_grandfather_pat']      = 'Abuelo Paterno';

$lang['thank'] =    '¡Gracias!';
$lang['message']    =   'Formulario de contacto enviado correctamente. ¡Gracias, nos pondremos en contacto con usted pronto!';
$lang['btn']    = 'Okay';

# PLACE HOLDER
$lang['year']      = 'año';
$lang['reason']      = 'Razón';
$lang['hospital']      = 'Hospital';
$lang['age']      = 'Edad';
$lang['significant']      = 'PROBLEMAS SIGNIFICATIVOS DE EDAD PROBLEMAS DE SALUD HEALTH';
$lang['days']      = 'dias';
$lang['home']   = 'Página de inicio';
$lang['site_title']     = 'Registro de pacientes en línea';
$lang['submit']      = 'Enviar';

$lang['accept']      = 'Acepto los <a href="#"> términos y condiciones </a>';

# LABEL
$lang['first_name']     = 'Primero Nombre';
$lang['mid_name']       = 'Segundo Nombre';
$lang['last_name']      = 'Apellido';
$lang['title']      = 'Título';
$lang['dob']      = 'Fecha de nacimiento';
$lang['sex']      = 'sexo';
$lang['street']      = 'Calle';
$lang['postal_code']      = 'Código postal';
$lang['city']      = 'Ciudad';
$lang['state']      = 'Estado';
$lang['marital_status']      = 'Estado civil';
$lang['prev_doctor']      = 'DOCTOR REFERENTE';
$lang['date_physical_exam']      = 'Fecha de último examen fisico';
$lang['chilhood_illness']      = 'ENFERMEDADES INFANTILES:';
$lang['immunization']      = 'Inmunizaciones y Fechas:';
$lang['medical_problems']      = 'Mencione las enfermedades que otros medicos le diagnosticaron';
$lang['surgeries']      = 'Cirugías';
$lang['other_hospital']      = 'Otras Hospitalizaciones';
$lang['blood_transfusion']      = '¿Alguna vez te han hecho una transfusión de sangre?';
$lang['prescribed_list']      = 'Mencione los medicamentos recetados de venta libre como vitaminas e inhaladores';
$lang['name_the_drug']      = 'Nombre de la droga';
$lang['strength']      = 'Fuerza';
$lang['frequency_taken']      = 'Frecuencia tomada';
$lang['reaction']      = 'Reacción que tuviste';

$lang['allergies']      = 'Alergico a algun medicamento';
$lang['medical_diet']      = '¿Estás a dieta?';
$lang['prescribed_medical_diet']      = 'En caso afirmativo, ¿Está dieta fue prescrita por un médico?';
$lang['prescribed_medical_diet2']      = '¿De las comidas que comes en un día promedio?';
$lang['salt_intake']      = 'Rango de consumo de sal';
$lang['fat_intake']      = 'Rango de ingesta de grasa';
$lang['cups_perday']      = '¿Cuantas tazas por día?';
$lang['drink_alcohol']      = '¿Bebes alcohol?';
$lang['alcohol_kind']      = 'En caso afirmativo, ¿de qué tipo?';
$lang['drink_perweek']      = '¿Cuántas bebidas por semana?';
$lang['drink_amount']      = '¿Te preocupa la cantidad que bebes?';
$lang['consider_stop']      = '¿Has considerado parar?';
$lang['blackout']      = '¿Algunas vez has experimentado congestion alcoholica?';
$lang['binge_drink']      = '¿Eres propenso a beber con exceso?';
$lang['after_drink']      = '¿Conduces después de beber?';
$lang['use_tobacco']      = '¿Usas tabaco?';
$lang['which_tobacco']      = 'Last';
$lang['street_drug']      = '¿Actualmente usas drogas recreativas o callejeras?';
$lang['drug_needle']      = '¿Algunas vez te has inyectado drogas?';
$lang['active_sex']      = '¿Eres sexualmente activo?';
$lang['pregnancy']      = 'En caso afirmativo, ¿has intentado embarazarte?';
$lang['contraceptive']      = 'Si no intenta embarzarse, ¿Que método de aticonceptivo usa?';
$lang['intercourse']      = '¿Alguna molestia con las relaciones sexuales?';
$lang['illness_related']      = 'Las enfermedades relacionadas con el Virus de la Inmunodeficiencia Humana (VIH), como el SIDA, se han convertido en un importante problema de salud pública. Los factores de riesgo de esta enfermedad incluyen el uso de drogas intravenosas y las relaciones sexuales sin protección. ¿Le gustaría hablar con su proveedor sobre su riesgo de padecer esta enfermedad?';
$lang['live_alone']      = '¿Vives solo?';
$lang['frequent_fall']      = '¿Tiene caídas frecuentes?';
$lang['vision_loss']      = '¿Tiene pérdida de visión o audición?';
$lang['living_will']      = '¿Tiene una Directiva Anticipada o Testamento Vital?';
$lang['information']      = '¿Desea información sobre la preparación de estos?';
$lang['mental_abuse']      = 'El abuso físico y/o mental también se ha convertido en un problema de salud pública importante en este país. Esto a menudo toma la forma de comportamiento verbalmente amenazante o abuso físico o sexual real. ¿Le gustaría discutir este problema con su proveedor?';
$lang['father_age']      = 'EDAD';
$lang['father_problem']      = 'PROBLEMAS SIGNIFICATIVOS DE EDAD PROBLEMAS DE SALUD HEALTH';
$lang['mother_age']      = 'EDAD';
$lang['mother_problem']      = 'PROBLEMAS SIGNIFICATIVOS DE EDAD PROBLEMAS DE SALUD HEALTH';
$lang['sibling_sex']      = 'Last';
$lang['sibling_problem']      = 'PROBLEMAS SIGNIFICATIVOS DE EDAD PROBLEMAS DE SALUD HEALTH';
$lang['children_sex']      = 'Last';
$lang['children_problem']      = 'PROBLEMAS SIGNIFICATIVOS DE EDAD PROBLEMAS DE SALUD HEALTH';
$lang['grandmother_age']      = 'EDAD';
$lang['grandmother_problem']      = 'PROBLEMAS SIGNIFICATIVOS DE EDAD PROBLEMAS DE SALUD HEALTH';
$lang['grandfather_age']      = 'EDAD';
$lang['grandfather_problem']      = 'PROBLEMAS SIGNIFICATIVOS DE EDAD PROBLEMAS DE SALUD HEALTH';
$lang['grandmother_pat_age']      = 'EDAD';
$lang['grandmother_pat_problem']      = 'PROBLEMAS SIGNIFICATIVOS DE EDAD PROBLEMAS DE SALUD HEALTH';
$lang['grandfather_pat_age']      = 'EDAD';
$lang['grandfather_pat_problem']      = 'PROBLEMAS SIGNIFICATIVOS DE EDAD PROBLEMAS DE SALUD HEALTH';
$lang['stress_problem']      = '¿Es el estrés un problema importante para usted?';
$lang['depressed']      = '¿Te sientes deprimido?';
$lang['panic_stressed']      = '¿Te asustas cuando estás estresado?';
$lang['eat_problem']      = '¿Tienes problemas alimenticios?';
$lang['cry_frequent']      = '¿Lloras con frecuencia?';
$lang['suicide']      = '¿Alguna vez intentaste suicidarte?';
$lang['self_hurting']      = '¿Alguna vez has pensado seriamente en lastimarte?';
$lang['trouble_sleep']      = '¿Tienes problemas para dormir?';
$lang['counselor']      = '¿Algunavez has estado con un consejero?';
$lang['age_menstruation']      = 'Edad al inicio de la menstruación:';
$lang['last_date_mens']      = 'Fecha de la última menstruación:';
$lang['periode_mens']      = 'Período cada';
$lang['heavy_periode']      = '¿Períodos intensos, irregularidad, manchado, dolor o secreción?';
$lang['number_pregnancy']      = 'Número de embarazos';
$lang['number_of_live']      = 'Número de nacimientos vivos';
$lang['are_you_pregnant']      = '¿Está embarazada o amamantando?';
$lang['had_dc']      = '¿Te has hecho una histerectomía o Cesárea?';
$lang['urinary_tract']      = '¿Alguna infección urinaria, vejiga o de riñon en el último año?';
$lang['urine_blood']      = '¿Tienes sangre en la orina?';
$lang['urinary_control']      = '¿Algún problema urinario?';
$lang['hot_flash']      = '¿Sufre de sudoración o sofocos por la noche?';
$lang['menstrual_tension']      = '¿Tiene tensión menstrual, dolor hinchazón, irritabilidad u otros sintomas en el periodo de menstruación?';
$lang['recent_breast']      = '¿Ha experimentado algún pecho sensible reciente, bultos o secreción del pezón?';
$lang['rectal_exam']      = '¿Fecha del último papanicolaou y examen rectal?';

$lang['urine_at_night']      = '¿Normalmente te levantas para orinar durante la noche?';
$lang['of_times_exam']      = 'En caso afirmativo, el número de veces';
$lang['feel_pain']      = '¿Sientes dolor o ardor al orinar?';
$lang['any_blood_in_urine']      = '¿Tienes sangre en la orina?';
$lang['feel_burning_penis']      = '¿¿Sientes ardor por secreción en el pene?';
$lang['urination_decrease']      = '¿Ha disminuido la fuerza de la eyaculación?';
$lang['infection_12']      = '¿Ha tenido alguna infección renal, vesical o de próstata en los últimos 12 meses?';
$lang['bladder_problem']      = '¿Tienes algún problema para vaciar la vejiga por completo?';
$lang['ejaculation']      = '¿Alguna dificultad con la erección o la eyaculación?';
$lang['testicle_pain']      = '¿Algún dolor o hinchazón en los testículos?';
$lang['last_prostate']      = '¿Fecha del último examen de próstata y recto?';
$lang['other_problem']      = 'Compruebe si tiene o ha tenido algún síntoma en las siguientes áreas en un grado significativo y explíquelo brevemente.';
$lang['recent_changes']      = 'Cambios recientes en';
$lang['other_pain']      = 'Otros dolores / molestias';

# LIST
$lang['arr_sex']['Male'] = 'Masculino';
$lang['arr_sex']['Female'] = 'Femenino';

$lang['arr_yesno']['YES'] = 'Sí';
$lang['arr_yesno']['NO'] = 'No';

$lang['arr_title']['Dr.'] = 'Dr.';
$lang['arr_title']['Mr.'] = 'Sr.';
$lang['arr_title']['Mrs.'] = 'Sra.';
$lang['arr_title']['Ms.'] = 'Srta.';

$lang['arr_marital_status']['divorced'] = 'Divorciada';
$lang['arr_marital_status']['domestic partner'] = 'Union Libre';
$lang['arr_marital_status']['married'] = 'Casada';
$lang['arr_marital_status']['separated'] = 'viuda';
$lang['arr_marital_status']['single'] = 'Soltera';
$lang['arr_marital_status']['widowed'] = 'Viuda';

$lang['arr_childhood']['Chickenpox'] = 'varicela';
$lang['arr_childhood']['Measles'] = 'Sarampión';
$lang['arr_childhood']['Mumps'] = 'paperas';
$lang['arr_childhood']['Polio'] = 'Polio';
$lang['arr_childhood']['Rheumatic Fever'] = 'fiebre reumática';
$lang['arr_childhood']['Rubella'] = 'Rubéola';

$lang['arr_immunization']['Chickenpox'] = 'Varicela';
$lang['arr_immunization']['Hepatitis'] = 'Hepatitis';
$lang['arr_immunization']['Influenza'] = 'Gripe';
$lang['arr_immunization']['MMR'] = 'MMR Sarampión, Paperas, Rubéola';
$lang['arr_immunization']['Pneumonia'] = 'Neumonía';
$lang['arr_immunization']['Tetanus'] = 'Tétanos';

$lang['arr_exercise']['Mild exercise'] = 'Nivel Leve (es decir , subir escaleras, caminar 3 cuadras)';
$lang['arr_exercise']['Occasional vigorous'] = 'Nivel Medio (es decir, trabajo o recreación, menos de 4 veces a la semana durante 30 minutos)';
$lang['arr_exercise']['Regular vigorous'] = 'Regular vigoroso exercise (es decir, trabajo o recreación 4x/semana durante 30 minutos)';
$lang['arr_exercise']['Sedentary'] = 'Sedentario (sin ejercicio)';

$lang['arr_rank_intake']['Hi'] = 'Alto';
$lang['arr_rank_intake']['Med'] = 'Medio';
$lang['arr_rank_intake']['Low'] = 'Bajo';

$lang['arr_caffeine']['Coffee'] = 'Café';
$lang['arr_caffeine']['Tea'] = 'Té';
$lang['arr_caffeine']['Cola'] = 'Bajo';
$lang['arr_caffeine']['None'] = 'Ninguno';

$lang['arr_tobaccos']['Chew'] = 'Masticar';
$lang['arr_tobaccos']['Cigarettes'] = 'Cigarros';
$lang['arr_tobaccos']['Cigars'] = 'Cigarros';
$lang['arr_tobaccos']['of years'] = 'Cuanto tiempo tiene Fumando';
$lang['arr_tobaccos']['or year quit'] = 'Año que dejo de fumar';
$lang['arr_tobaccos']['Pipe'] = 'Pipa';

$lang['arr_problem']['Ability to sleep'] = 'Capacidad para dormir';
$lang['arr_problem']['Back'] = 'Espalda';
$lang['arr_problem']['Bladder'] = 'Vejiga';
$lang['arr_problem']['Bowel'] = 'Intestino';
$lang['arr_problem']['Chest/Heart'] = 'Pecho/Corazón';
$lang['arr_problem']['Circulation'] = 'Circulación';
$lang['arr_problem']['Ears'] = 'Oídos';
$lang['arr_problem']['Energy level'] = 'Nivel de energia';
$lang['arr_problem']['Head/Neck'] = 'Cabeza/Cuello';
$lang['arr_problem']['Intestinal'] = 'Intestinal';
$lang['arr_problem']['Lungs'] = 'Pulmones';
$lang['arr_problem']['Nose'] = 'Nariz';
$lang['arr_problem']['Other pain/discomfort'] = 'Otro Dolor o inconformidad';
$lang['arr_problem']['Recent changes in'] = 'C recientecuelga in:';
$lang['arr_problem']['Skin'] = 'Piel';
$lang['arr_problem']['Throat'] = 'Garganta';
$lang['arr_problem']['Weight'] = 'Peso';



