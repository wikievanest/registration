<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

# HEADER 
$lang['language']      = 'Select Language';
$lang['header']     = 'HEALTH HISTORY QUESTIONNAIRE';
$lang['desc']       = 'All questions contained in this questionnaire are strictly confidential and will become part of your medical record.';

# LEGEND
$lang['leg_personal']      = 'Personal Informations';
$lang['leg_health_history']      = 'Personal Health History';
$lang['leg_health_habbit']      = 'Health Habits and Personal Safety';
$lang['leg_health_habbit_desc']       = 'ALL QUESTIONS CONTAINED IN THIS QUESTIONNAIRE ARE OPTIONAL AND WILL BE KEPT STRICTLY CONFIDENTIAL.';
$lang['leg_family']      = 'FAMILY HEALTH HISTORY';
$lang['leg_mental_health']      = 'Mental Health';
$lang['leg_women_only']      = 'Women Only';
$lang['leg_men_only']      = 'Men Only';
$lang['leg_other_problem']      = 'Other Problems';

# SECTION

$lang['sec_exercise']      = 'EXERCISE';
$lang['sec_diet']      = 'DIET';
$lang['sec_caffeine']      = 'CAFFEINE';
$lang['sec_alcohol']      = 'ALCOHOL';
$lang['sec_tobacco']      = 'TOBACCO';
$lang['sec_drugs']      = 'DRUGS';
$lang['sec_sex']      = 'SEX';
$lang['sec_father']      = 'FATHER';
$lang['sec_mother']      = 'MOTHER';
$lang['sec_sibling']      = 'SIBLING';
$lang['sec_children']      = 'CHILDREN';
$lang['sec_grandmother_mat']      = 'GRANDMOTHER';
$lang['sec_grandfather_mat']      = 'GRANDFATHER';
$lang['sec_grandmother_pat']      = 'GRANDMOTHER';
$lang['sec_grandfather_pat']      = 'GRANDFATHER';
$lang['sec_personal_safety']      = 'PERSONAL SAFETY';
$lang['site_title']     = 'Online Patient Registration';
$lang['home']   = 'Home';

# PLACE HOLDER
$lang['year']      = 'Year';
$lang['reason']      = 'Reason';
$lang['hospital']      = 'Hospital';
$lang['age']      = 'Age';
$lang['significant']      = 'Significant Health Problems';
$lang['days']      = 'Days';
$lang['submit']      = 'Submit';
$lang['accept']      = 'I accept the <a href="#">terms and conditions</a>';

$lang['thank'] =    'Thank you!';
$lang['message']    =   'Contact form successfully submitted. Thank you, We will get back to you soon!.';
$lang['btn']    = 'OK';

# LABEL
$lang['first_name']     = 'First Name';
$lang['mid_name']       = 'Middle Name';
$lang['last_name']      = 'Last Name';
$lang['title']      = 'Title';
$lang['dob']      = 'DOB';
$lang['sex']      = 'Sex';
$lang['street']      = 'Street';
$lang['postal_code']      = 'Postal Code';
$lang['city']      = 'City';
$lang['state']      = 'State';
$lang['marital_status']      = 'Marital Status';
$lang['prev_doctor']      = 'Previous or referring doctor';
$lang['date_physical_exam']      = 'Date of last physical exam';
$lang['chilhood_illness']      = 'Childhood illness';
$lang['immunization']      = 'Immunizations and dates?';
$lang['medical_problems']      = 'List any medical problems that other doctors have diagnosed';
$lang['surgeries']      = 'Surgeries';
$lang['other_hospital']      = 'Other hospitalizations';
$lang['blood_transfusion']      = 'Have you ever had a blood transfusion';
$lang['prescribed_list']      = 'List your prescribed drugs and over-the-counter drugs, such as vitamins and inhalers';
$lang['name_the_drug']      = 'Name the Drug';
$lang['strength']      = 'Strength';
$lang['frequency_taken']      = 'Frequency Taken';
$lang['strength']      = 'Strength';
$lang['reaction']      = 'Reaction you had';
$lang['allergies']      = 'Allergies to medications';
$lang['medical_diet']      = 'Are you dieting?';
$lang['prescribed_medical_diet']      = 'If YES, are you on a physician prescribed medical diet?';
$lang['prescribed_medical_diet2']      = '# of meals you eat in an average day?';
$lang['salt_intake']      = 'Rank salt intake';
$lang['fat_intake']      = 'Rank fat intake';
$lang['cups_perday']      = '# of cups/cans per day?';
$lang['drink_alcohol']      = 'Do you drink alcohol?';
$lang['alcohol_kind']      = 'If YES, what kind?';
$lang['drink_perweek']      = 'How many drinks per week?';
$lang['drink_amount']      = 'Are you concerned about the amount you drink?';
$lang['consider_stop']      = 'Have you considered stopping?';
$lang['blackout']      = 'Have you ever experienced blackouts?';
$lang['binge_drink']      = 'Are you prone to "binge" drinking?';
$lang['after_drink']      = 'Do you drive after drinking?';
$lang['use_tobacco']      = 'Do you use tobacco?';
$lang['which_tobacco']      = 'Last';
$lang['street_drug']      = 'Do you currently use recreational or street drugs?';
$lang['drug_needle']      = 'Have you ever given yourself street drugs with a needle?';
$lang['active_sex']      = 'Are you sexually active?';
$lang['pregnancy']      = 'If YES, are you trying for a pregnancy?';
$lang['contraceptive']      = 'If NOt trying for a pregnancy list contraceptive or barrier method used';
$lang['intercourse']      = 'Any discomfort with intercourse?';
$lang['illness_related']      = 'Illness related to the Human ImmuNOdeficiency Virus (HIV), such as AIDS, has become a major public health problem. Risk factors for this illness include intraveNOus drug use and unprotected sexual intercourse. Would you like to speak with your provider about your risk of this illness?';
$lang['live_alone']      = 'Do you live alone?';
$lang['frequent_fall']      = 'Do you have frequent falls?';
$lang['vision_loss']      = 'Do you have vision or hearing loss?';
$lang['living_will']      = 'Do you have an Advance Directive or Living Will?';
$lang['information']      = 'Would you like information on the preparation of these?';
$lang['mental_abuse']      = 'Physical and/or mental abuse have also become major public health issues in this country. This often takes the form of verbally threatening behavior or actual physical or sexual abuse. Would you like to discuss this issue with your provider?';
$lang['father_age']      = 'Age';
$lang['father_problem']      = 'Significant Health Problems';
$lang['mother_age']      = 'Age';
$lang['mother_problem']      = 'Significant Health Problems';
$lang['sibling_sex']      = 'Last';
$lang['sibling_problem']      = 'Significant Health Problems';
$lang['children_sex']      = 'Last';
$lang['children_problem']      = 'Significant Health Problems';
$lang['grandmother_age']      = 'Age';
$lang['grandmother_problem']      = 'Significant Health Problems';
$lang['grandfather_age']      = 'Age';
$lang['grandfather_problem']      = 'Significant Health Problems';
$lang['grandmother_pat_age']      = 'Age';
$lang['grandmother_pat_problem']      = 'Significant Health Problems';
$lang['grandfather_pat_age']      = 'Age';
$lang['grandfather_pat_problem']      = 'Significant Health Problems';
$lang['stress_problem']      = 'Is stress a major problem for you?';
$lang['depressed']      = 'Do you feel depressed?';
$lang['panic_stressed']      = 'Do you panic when stressed?';
$lang['eat_problem']      = 'Do you have problems with eating or your appetite?';
$lang['cry_frequent']      = 'Do you cry frequently?';
$lang['suicide']      = 'Have you ever attempted suicide?';
$lang['self_hurting']      = 'Have you ever seriously thought about hurting yourself?';
$lang['trouble_sleep']      = 'Do you have trouble sleeping?';
$lang['counselor']      = 'Have you ever been to a counselor?';
$lang['age_menstruation']      = 'Age at onset of menstruation';
$lang['last_date_mens']      = 'Date of last menstruation';
$lang['periode_mens']      = 'Period every';
$lang['heavy_periode']      = 'Heavy periods, irregularity, spotting, pain, or discharge?';
$lang['number_pregnancy']      = 'Number of pregnancies';
$lang['number_of_live']      = 'Number of live births';
$lang['are_you_pregnant']      = 'Are you pregnant or breastfeeding?';
$lang['had_dc']      = 'Have you had a D&C, hysterectomy, or Cesarean?';
$lang['urinary_tract']      = 'Any urinary tract, bladder, or kidney infections within the last year?';
$lang['urine_blood']      = 'Any blood in your urine?';
$lang['urinary_control']      = 'Any problems with control of urination?';
$lang['hot_flash']      = 'Any hot flashes or sweating at night?';
$lang['menstrual_tension']      = 'Do you have menstrual tension, pain, bloating, irritability, or other symptoms at or around time of period?';
$lang['recent_breast']      = 'Experienced any recent breast tenderness, lumps, or nipple discharge?';
$lang['rectal_exam']      = 'Date of last pap and rectal exam?';

$lang['urine_at_night']      = 'Do you usually get up to urinate during the night?';
$lang['of_times_exam']      = 'If YES, # of times';
$lang['feel_pain']      = 'Do you feel pain or burning with urination?';
$lang['any_blood_in_urine']      = 'Any blood in your urine?';
$lang['feel_burning_penis']      = 'Do you feel burning discharge from penis?';
$lang['urination_decrease']      = 'Has the force of your urination decreased?';
$lang['infection_12']      = 'Have you had any kidney, bladder, or prostate infections within the last 12 months?';
$lang['bladder_problem']      = 'Do you have any problems emptying your bladder completely?';
$lang['ejaculation']      = 'Any difficulty with erection or ejaculation?';
$lang['testicle_pain']      = 'Any testicle pain or swelling?';
$lang['last_prostate']      = 'Date of last prostate and rectal exam?';
$lang['other_problem']      = 'Check if you have, or have had, any symptoms in the following areas to a significant degree and briefly explain.';
$lang['recent_changes']      = 'Recent changes in';
$lang['other_pain']      = 'Other pain/discomfort';

# LIST
$lang['arr_sex']['Male'] = 'Male';
$lang['arr_sex']['Female'] = 'Female';

$lang['arr_yesno']['YES'] = 'YES';
$lang['arr_yesno']['NO'] = 'NO';

$lang['arr_title']['Dr.'] = 'Dr.';
$lang['arr_title']['Mr.'] = 'Mr.';
$lang['arr_title']['Mrs.'] = 'Mrs.';
$lang['arr_title']['Ms.'] = 'Ms.';

$lang['arr_marital_status']['divorced'] = 'Divorced';
$lang['arr_marital_status']['domestic partner'] = 'Domestic Partner';
$lang['arr_marital_status']['married'] = 'Married';
$lang['arr_marital_status']['separated'] = 'Separated';
$lang['arr_marital_status']['single'] = 'Single';
$lang['arr_marital_status']['widowed'] = 'Widowed';

$lang['arr_childhood']['Chickenpox'] = 'Chickenpox';
$lang['arr_childhood']['Measles'] = 'Measles';
$lang['arr_childhood']['Mumps'] = 'Mumps';
$lang['arr_childhood']['Polio'] = 'Polio';
$lang['arr_childhood']['Rheumatic Fever'] = 'Rheumatic Fever';
$lang['arr_childhood']['Rubella'] = 'Rubella';

$lang['arr_immunization']['Chickenpox'] = 'Chickenpox';
$lang['arr_immunization']['Hepatitis'] = 'Hepatitis';
$lang['arr_immunization']['Influenza'] = 'Influenza';
$lang['arr_immunization']['MMR'] = 'MMR Measles, Mumps, Rubella';
$lang['arr_immunization']['Pneumonia'] = 'Pneumonia';
$lang['arr_immunization']['Tetanus'] = 'Tetanus';

$lang['arr_exercise']['Mild exercise'] = 'Mild exercise (i.e., climb stairs, walk 3 blocks, golf)';
$lang['arr_exercise']['Occasional vigorous'] = 'Occasional vigorous exercise (i.e., work or recreation, less than 4x/week for 30 min.)';
$lang['arr_exercise']['Regular vigorous'] = 'Regular vigorous exercise (i.e., work or recreation 4x/week';
$lang['arr_exercise']['Sedentary'] = 'Sedentary (No exercise)';

$lang['arr_rank_intake']['Hi'] = 'Hi';
$lang['arr_rank_intake']['Med'] = 'Med';
$lang['arr_rank_intake']['Low'] = 'Low';

$lang['arr_caffeine']['Coffee'] = 'Coffee';
$lang['arr_caffeine']['Cola'] = 'Cola';
$lang['arr_caffeine']['Tea'] = 'Tea';
$lang['arr_caffeine']['None'] = 'None';

$lang['arr_tobaccos']['Chew'] = 'Chew - #/day';
$lang['arr_tobaccos']['Cigarettes'] = 'Cigarettes – pks./day';
$lang['arr_tobaccos']['Cigars'] = 'Cigars';
$lang['arr_tobaccos']['of years'] = '# of years';
$lang['arr_tobaccos']['or year quit'] = 'Or year quit';
$lang['arr_tobaccos']['Pipe'] = 'Pipe - #/day';

$lang['arr_problem']['Ability to sleep'] = 'Ability to sleep';
$lang['arr_problem']['Back'] = 'Back';
$lang['arr_problem']['Bladder'] = 'Bladder';
$lang['arr_problem']['Bowel'] = 'Bowel';
$lang['arr_problem']['Chest/Heart'] = 'Chest/Heart';
$lang['arr_problem']['Circulation'] = 'Circulation';
$lang['arr_problem']['Ears'] = 'Ears';
$lang['arr_problem']['Energy level'] = 'Energy level';
$lang['arr_problem']['Head/Neck'] = 'Head/Neck';
$lang['arr_problem']['Intestinal'] = 'Intestinal';
$lang['arr_problem']['Lungs'] = 'Lungs';
$lang['arr_problem']['Nose'] = 'Nose';
$lang['arr_problem']['Other pain/discomfort'] = 'Other pain/discomfort';
$lang['arr_problem']['Recent changes in'] = 'Recent changes in';
$lang['arr_problem']['Skin'] = 'Skin';
$lang['arr_problem']['Throat'] = 'Throat';
$lang['arr_problem']['Weight'] = 'Weight';


