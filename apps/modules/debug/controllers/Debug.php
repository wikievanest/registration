<?php if ( ! defined('BASEPATH')) exit('No direct script auth allowed');
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmapp/apps/modules/debug/controllers/Debug.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-05-25 22:07:47
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-05-10 01:17:40
 */

class Debug extends MY_Controller{
    protected $data = '';
    
    public function __construct() {
        parent::__construct();
        $this->data = array(
            'home' => site_url(),
            'msg_main' => "ERROR 403",
            'msg_detail' => "Request Page not found",
        );
    }
    
    public function index() {
        $this->err_404();
    }
    
    public function err_403() {
        $this->template
            ->title('Error 403','Access Forbidden')
            ->set_layout('main')
            ->build('debug/err_403',$this->data);
    }
}
