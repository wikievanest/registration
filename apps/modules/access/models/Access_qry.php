<?php
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmapp7/apps/modules/access/models/Access_qry.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-05-25 22:07:47
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-09-02 03:09:05
 */

class Access_qry extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function login($email,$password) {
        $hash = $this->get_hash_password($email);
        if (password_verify($password, $hash->password)) {
            $data = array(
            'user_id',
            'first_name',
            'last_name',
            'email',
            'password',
            'level',
            'user_stat'
        );
        $this->db->select($data);
        $where = array(
            'email' => $email,
            'password' => $hash->password
        );
        $this->db->where($where);
        $query = $this->db->get("USERS");
        $row = $query->num_rows();
            // if user found with email and password

            if($row>0) {
                $data = array(
                    'user_id',
                    'first_name',
                    'last_name',
                    'email',
                    'password',
                    'level',
                    'user_stat'
                );
                $this->db->select($data);
                $this->db->where('email', $email);
                $this->db->where('password', $hash->password);
                $query2 = $this->db->get("USERS");
                $row2 = $query2->row();

                // is status user 1
                if($row2->user_stat==1) {

                    // Check if user is agent

                    if($row2->level==5) {
                        $sql = "SELECT
                              a.USER_ID,
                              a.EMR_ID,
                              a.ACTIVE,
                              b.EMR_NAME,
                              b.EMR_CLIENT
                            FROM EMRS_USERS a
                            INNER JOIN EMRS b
                              ON b.EMR_ID = a.EMR_ID
                            WHERE a.ACTIVE = 1
                            AND USER_ID = '$row2->user_id'";
                        $qry = $this->db->query($sql);

                        $res = $qry->row();
                        if(($res->EMR_NAME==$this->emrs->emr) && ($res->EMR_CLIENT==$this->emrs->client)) {
                            $data = array(
                                'user_id' => $row2->user_id,
                                'first_name' => $row2->first_name,
                                'last_name' => $row2->last_name,
                                'email' => $row2->email,
                                'level' => $row2->level,
                                'user_stat' => $row2->user_stat,
                                'is_login' => true
                            );
                            $this->session->set_userdata($data);
                            return 1; // User Match
                        } else{
                            return 9;
                            exit;
                        }
                    }
                    
                    $data = array(
                        'user_id' => $row2->user_id,
                        'first_name' => $row2->first_name,
                        'last_name' => $row2->last_name,
                        'email' => $row2->email,
                        'level' => $row2->level,
                        'user_stat' => $row2->user_stat,
                        'is_login' => true
                    );
                    $this->session->set_userdata($data);
                    return 1; // User Match
                } else {
                    return 9; // User Not Active
                }
            } else {
                $this->session->set_flashdata('item', array('message' => 'Login Failed | Email or Password not match','class' => 'warning'));
            }
               
        } else {
            return 0; // Password not match
        }
        
    }

    public function get_hash_password($email) {
        $data = array(
            'password'
        );
        $this->db->select($data);
        $this->db->where('email', $email);
        $query = $this->db->get("USERS");
        $result = $query->row();
        return $result;
    }
}
