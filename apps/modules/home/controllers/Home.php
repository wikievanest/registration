<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmappbruce/apps/modules/home/controllers/Home.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-07-19 11:04:00
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-08-17 16:14:52
 */


class Home extends MY_Controller {
    var $title;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Home_qry','home');
        $this->title = 'Dashboard';
    }

    public function index() {   
        $data['yesno'] = $this->home->get_lang('arr_yesno');
        $data['titles'] = $this->home->get_lang('arr_title');
        $data['state'] = $this->home->get_lists('state');
        $data['marital'] = $this->home->get_lang('arr_marital_status');
        $data['sex'] = $this->home->get_lang('arr_sex');
        $data['childhood'] = $this->home->get_lang('arr_childhood');
        $data['immunization'] = $this->home->get_lang('arr_immunization');
        $data['exercise'] = $this->home->get_lang('arr_exercise');
        $data['rank_intake'] = $this->home->get_lang('arr_rank_intake');
        $data['caffein'] = $this->home->get_lang('arr_caffeine');
        $data['tobaccos'] = $this->home->get_lang('arr_tobaccos');
        $data['other_problems'] = $this->home->get_lang('arr_problem');
        $this->template
                    ->title($this->home->get_lang('home'),$this->home->get_lang('site_title'))
                    ->set_layout('main')
                    ->build('index', $data);
        
    }


    public function success() {   
        $this->template
                    ->title($this->home->get_lang('home'),$this->home->get_lang('site_title'))
                    ->set_layout('main')
                    ->build('thanks');
        
    }

    private function getToken() {
        $data_array =  array(
            "username"  => 'admin',
            "password"  => 'uSTM2019#',
        );
      
      $make_call = $this->api->getToken('POST', 'https://api.emrforhospitals.com/v1/login/', json_encode($data_array));
      $response = json_decode($make_call, true);
      $errors   = $response['response']['errors'];
      $token     = $response['token'];
      if($token) {
          return $token;
      }
    }

    public function getPatient() {

        $token = $this->getToken();
        $make_call = $this->api->callAPI('GET', 'https://api.emrforhospitals.com/v1/patients/', '',$token);
        $response = json_decode($make_call, true);
        var_dump($response);
        exit;
        $errors   = $response['response']['errors'];
        $token     = $response['token'];
    }

    public function process() {

        $first_name = $this->input->post('first_name');
        $mid_name = $this->input->post('mid_name');
        $last_name = $this->input->post('last_name');
        $dob = $this->input->post('DOB');
        $sex = $this->input->post('gender');
        $title = $this->input->post('title');
        $street = $this->input->post('street');
        $postal_code = $this->input->post('postal_code');
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $marital_status = $this->input->post('marital_status');
        $prev_doctor = $this->input->post('prev_doctor');
        $date_physical_exam = $this->input->post('date_physical_exam');
        $chilhood_illness = $this->input->post('chilhood_illness');
        $immunization = implode(',',$this->input->post('immunization'));
        $medical_problems = $this->input->post('medical_problems');
        $surgeries_year = $this->input->post('surgeries_year');
        $surgeries_reason = $this->input->post('surgeries_reason');
        $surgeries_hospital = $this->input->post('surgeries_hospital');
        $other_hospital_year = $this->input->post('other_hospital_year');
        $other_hospital_reason = $this->input->post('other_hospital_reason');
        $other_hospital_hospital = $this->input->post('other_hospital_hospital');
        $blood_transfusion = $this->input->post('blood_transfusion');
        $prescribed_list_drug_name = $this->input->post('prescribed_list_drug_name');
        $prescribed_list_strength = $this->input->post('prescribed_list_strength');
        $prescribed_list_frequency_taken = $this->input->post('prescribed_list_frequency_taken');
        $allergies_drug_name = $this->input->post('allergies_drug_name');
        $allergies_reaction = $this->input->post('allergies_reaction');
        $exercise = $this->input->post('exercise');
        $diet = $this->input->post('diet');
        $medical_diet = $this->input->post('medical_diet');
        $eat_average = $this->input->post('eat_average');
        $salt_intake = $this->input->post('salt_intake');
        $fat_intake = $this->input->post('fat_intake');
        $caffeine = $this->input->post('caffeine');
        $cups_perday = $this->input->post('cups_perday');
        $drink_alcohol = $this->input->post('drink_alcohol');
        $alcohol_kind = $this->input->post('alcohol_kind');
        $drink_perweek = $this->input->post('drink_perweek');
        $drink_amount = $this->input->post('drink_amount');
        $consider_stop = $this->input->post('consider_stop');
        $blackout = $this->input->post('blackout');
        $binge_drink = $this->input->post('binge_drink');
        $after_drink = $this->input->post('after_drink');
        $use_tobacco = $this->input->post('use_tobacco');
        $which_tobacco = $this->input->post('which_tobacco');
        $street_drug = $this->input->post('street_drug');
        $drug_needle = $this->input->post('drug_needle');
        $active_sex = $this->input->post('active_sex');
        $pregnancy = $this->input->post('pregnancy');
        $contraceptive = $this->input->post('contraceptive');
        $intercourse = $this->input->post('intercourse');
        $illness_related = $this->input->post('illness_related');
        $live_alone = $this->input->post('live_alone');
        $frequent_fall = $this->input->post('frequent_fall');
        $vision_loss = $this->input->post('vision_loss');
        $living_will = $this->input->post('living_will');
        $information = $this->input->post('information');
        $mental_abuse = $this->input->post('mental_abuse');
        $father_age = $this->input->post('father_age');
        $father_problem = $this->input->post('father_problem');
        $mother_age = $this->input->post('mother_age');
        $mother_problem = $this->input->post('mother_problem');
        $sibling_sex = $this->input->post('sibling_sex');
        $sibling_problem = $this->input->post('sibling_problem');
        $children_sex = $this->input->post('children_sex');
        $children_problem = $this->input->post('children_problem');
        $grandmother_age = $this->input->post('grandmother_age');
        $grandmother_problem = $this->input->post('grandmother_problem');
        $grandfather_age = $this->input->post('grandfather_age');
        $grandfather_problem = $this->input->post('grandfather_problem');
        $grandmother_pat_age = $this->input->post('grandmother_pat_age');
        $grandmother_pat_problem = $this->input->post('grandmother_pat_problem');
        $grandfather_pat_age = $this->input->post('grandfather_pat_age');
        $grandfather_pat_problem = $this->input->post('grandfather_pat_problem');
        $stress_problem = $this->input->post('stress_problem');
        $depressed = $this->input->post('depressed');
        $panic_stressed = $this->input->post('panic_stressed');
        $eat_problem = $this->input->post('eat_problem');
        $cry_frequent = $this->input->post('cry_frequent');
        $suicide = $this->input->post('suicide');
        $self_hurting = $this->input->post('self_hurting');
        $trouble_sleep = $this->input->post('trouble_sleep');
        $counselor = $this->input->post('counselor');
        $age_menstruation = $this->input->post('age_menstruation');
        $last_date_mens = $this->input->post('last_date_mens');
        $periode_mens = $this->input->post('periode_mens');
        $heavy_periode = $this->input->post('heavy_periode');
        $number_pregnancy = $this->input->post('number_pregnancy');
        $number_of_live = $this->input->post('number_of_live');
        $are_you_pregnant = $this->input->post('are_you_pregnant');
        $had_dc = $this->input->post('had_dc');
        $urinary_tract = $this->input->post('urinary_tract');
        $urine_blood = $this->input->post('urine_blood');
        $urinary_control = $this->input->post('urinary_control');
        $hot_flash = $this->input->post('hot_flash');
        $menstrual_tension = $this->input->post('menstrual_tension');
        $recent_breast = $this->input->post('recent_breast');
        $rectal_exam = $this->input->post('rectal_exam');
        $of_times_exam = $this->input->post('of_times_exam');
        $feel_pain = $this->input->post('feel_pain');
        $any_blood_in_urine = $this->input->post('any_blood_in_urine');
        $feel_burning_penis = $this->input->post('feel_burning_penis');
        $urination_decrease = $this->input->post('urination_decrease');
        $infection_12 = $this->input->post('infection_12');
        $bladder_problem = $this->input->post('bladder_problem');
        $ejaculation = $this->input->post('ejaculation');
        $testicle_pain = $this->input->post('testicle_pain');
        $last_prostate = $this->input->post('last_prostate');
        $urine_at_night = $this->input->post('urine_at_night');
        $other_problem = $this->input->post('other_problem');
        $recent_changes = $this->input->post('recent_changes');
        $other_pain = $this->input->post('other_pain');

        $primary =  array(
            "fname"  =>  $first_name,
            "mname"  =>  $mid_name,
            "lname"  =>  $last_name,
            "dob"  =>  $dob,
            "sex"  =>  $sex,
            "title"  =>  $title,
            "status"  =>  $marital_status,
            "street"  =>  $street,
            "city"  =>  $city,
            "state"  =>  $state,
            "postal_code"  =>  $postal_code
        );

        $secondary =  array(
            "referring_doctor"  =>  $prev_doctor,
            "physical_exam_date"  =>  $date_physical_exam,
            "chilhood_illness"  =>  $chilhood_illness,
            "immunization"  =>  $immunization,
            "any_medical_problem"  =>  $medical_problems,
            "surgeries_year1"  =>  $surgeries_year,
            "surgeries_reason1"  =>  $surgeries_reason,
            "surgeries_hospital"  =>  $surgeries_hospital,
            "surgeries_year2"  =>  $other_pain,
            "surgeries_reason2"  =>  $other_pain,
            "surgeries_hospital2"  =>  $other_pain,
            "surgeries_year3"  =>  $other_pain,
            "surgeries_reason3"  =>  $other_pain,
            "surgeries_hospital3"  =>  $other_pain,
            "hospitalizations1"  =>  $other_hospital_year,
            "hospital_reason1"  =>  $other_hospital_reason,
            "hospital_hospital1"  =>  $other_hospital_hospital,
            "ever_had_blood"  =>  $blood_transfusion,
            "name_the_drug1"  =>  $prescribed_list_drug_name,
            "strength1"  =>  $prescribed_list_strength,
            "frequency_taken1"  =>  $prescribed_list_frequency_taken,
            "name_the_drug2"  =>  $allergies_drug_name,
            "strength2"  =>  $allergies_reaction,
            "frequency_taken2"  =>  $other_pain,
            "name_the_drug_allerg"  =>  $other_pain,
            "drug_reaction1"  =>  $other_pain,
            "name_the_drug_aller2"  =>  $other_pain,
            "drug_reaction2"  =>  $other_pain,
            "Exercise"  =>  $exercise,
            "diet"  =>  $diet,
            "physician_medical"  =>  $medical_diet,
            "rank_salt_intake"  =>  $salt_intake,
            "rank_fat_intake"  =>  $fat_intake,
            "caffeine"  =>  $caffeine,
            "cup_perday"  =>  $cups_perday,
            "drink_alcohol"  =>  $drink_alcohol,
            "what_kind"  =>  $alcohol_kind,
            "drink_per_week"  =>  $drink_perweek,
            "drink_amount"  =>  $drink_amount,
            "consider_stopping"  =>  $consider_stop,
            "experience_blackout"  =>  $blackout,
            "binge_drinking"  =>  $binge_drink,
            "drive_after_drink"  =>  $after_drink,
            "use_tobacco"  =>  $use_tobacco,
            "which_tobacco"  =>  $which_tobacco,
            "street_drugs"  =>  $street_drug,
            "given_street_drugs"  =>  $drug_needle,
            "sexually_active"  =>  $active_sex,
            "try_pregnancy"  =>  $pregnancy,
            "contraceptive_method"  =>  $contraceptive,
            "discomfort_intercour"  =>  $intercourse,
            "illness_related"  =>  $illness_related,
            "live_alone"  =>  $live_alone,
            "frequent_falls"  =>  $frequent_fall,
            "vision_hear_loss"  =>  $vision_loss,
            "advance_directive"  =>  $living_will,
            "preparation_info"  =>  $information,
            "mental_abuse"  =>  $mental_abuse,
            "father_age1"  =>  $father_age,
            "father_problem"  =>  $father_problem,
            "mother_age"  =>  $mother_age,
            "mother_problem"  =>  $mother_problem,
            "sibling_sex"  =>  $sibling_sex,
            "sibling_problem"  =>  $sibling_problem,
            "children_sex"  =>  $children_sex,
            "children_problem"  =>  $children_problem,
            "grandmother_age"  =>  $grandmother_age,
            "grandmother_problem"  =>  $grandmother_problem,
            "grandfather_age"  =>  $grandfather_age,
            "grandfather_problem"  =>  $grandfather_problem,
            "grandmother_pat_age"  =>  $grandmother_pat_age,
            "grandmother_pat_prob"  =>  $grandmother_pat_problem,
            "grandfather_pat_age"  =>  $grandfather_pat_age,
            "grandfather_pat_prob"  =>  $grandfather_pat_problem,
            "stress_problem"  =>  $stress_problem,
            "feel_depressed"  =>  $depressed,
            "panic_stress"  =>  $panic_stressed,
            "eat_problem"  =>  $eat_problem,
            "cry_frequently"  =>  $cry_frequent,
            "attempted_suicide"  =>  $suicide,
            "seriuosly_thought"  =>  $self_hurting,
            "trouble_sleeping"  =>  $trouble_sleep,
            "to_conselor"  =>  $counselor,
            "age_onset_mens"  =>  $age_menstruation,
            "date_last_mens"  =>  $last_date_mens,
            "how_many_day"  =>  $periode_mens,
            "heavy_periods"  =>  $heavy_periode,
            "number_of_pregnancy"  =>  $number_pregnancy,
            "number_of_live"  =>  $number_of_live,
            "is_pregnant"  =>  $are_you_pregnant,
            "had_dc_hysterectomy"  =>  $had_dc,
            "any_urinary_tract"  =>  $urinary_tract,
            "any_blood"  =>  $urine_blood,
            "any_problems"  =>  $urinary_control,
            "any_hot_flash"  =>  $hot_flash,
            "have_mens_tension"  =>  $menstrual_tension,
            "recent_breast"  =>  $recent_breast,
            "date_last_pap"  =>  $rectal_exam,
            "urinate_the_night"  =>  $urine_at_night,
            "how_many_urinate"  =>  $other_pain,
            "feel_pain_urination"  =>  $feel_pain,
            "blood_in_urine"  =>  $any_blood_in_urine,
            "feel_burn_penis"  =>  $feel_burning_penis,
            "has_force_urination"  =>  $urination_decrease,
            "had_any_kidney"  =>  $infection_12,
            "any_problem_bladder"  =>  $bladder_problem,
            "difficulty_erection"  =>  $ejaculation,
            "testicle_pain"  =>  $testicle_pain,
            "last_prostate"  =>  $last_prostate,
            "any_symptoms"  =>  implode(',',$other_problem),
        );

        $token = $this->getToken();
        $make_call = $this->api->callAPI('POST', 'https://api.emrforhospitals.com/v1/patients/', json_encode($primary), $token);
        $response = json_decode($make_call, true);
        $id = $response['id'];
        if($id != '') {
            $sec = $this->api->callAPI('PUT', 'https://api.emrforhospitals.com/v1/patients/'.$id.'/', json_encode($secondary), $token);
            $resp = json_decode($sec, true);
            if(is_array($resp)) {
                redirect('/success');
            }
        }
    }

    public function change_language($language = "") {

        $language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);
       
        redirect($_SERVER['HTTP_REFERER']);
       
    }
    
}
