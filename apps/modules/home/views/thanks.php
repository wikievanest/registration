<script>
    $(document).ready(function(){
        $("#myModal").modal('show');
    });
</script>
<div id="myModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">		
				<h4 class="modal-title"><?php echo $this->lang->line('thank'); ?></h4>	
			</div>
			<div class="modal-body">
				<p class="text-center"><?php echo $this->lang->line('message'); ?></p>
			</div>
			<div class="modal-footer">
				<a href="<?=base_url('home');?>" class="btn btn-success btn-block"><?php echo $this->lang->line('btn'); ?></a>
			</div>
		</div>
	</div>
</div>     