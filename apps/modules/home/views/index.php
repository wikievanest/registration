<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2" style="margin-top: 25px">
      <?php
      $submit = "process";
      $attributes = array(
        'role' => 'form', 'id' => 'form_add', 'name' => 'form_add', 'onSubmit' => 'document.getElementById(\'btn\').disabled=true;'
      );
      echo form_open($submit, $attributes);
      ?>
      <legend class="text-center"><?php echo $this->lang->line('header'); ?></legend>
      <legend class="text-center"><?php echo $this->lang->line('desc'); ?></legend>
      <fieldset>
        <legend><?php echo $this->lang->line('leg_personal'); ?></legend>

        <div class="form-group col-md-4">
          <?php echo lang('first_name', 'first_name'); ?>
          <input type="text" class="form-control" name="first_name" required>
        </div>

        <div class="form-group col-md-3">
        <?php echo lang('mid_name', 'mid_name'); ?>
          <input type="text" class="form-control" name="mid_name"required>
        </div>
        <div class="form-group col-md-5">
        <?php echo lang('last_name', 'last_name'); ?>
          <input type="text" class="form-control" name="last_name"required>
        </div>
        <div class="form-group col-md-4">
        <?php echo lang('title', 'title'); ?>
          <select name="title" class="form-control">
          <?php
            foreach ($titles as $key=>$val) {
              ?>
              <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
            <?php
            }
            ?>
          </select>
        </div>
        <div class="form-group col-md-4">
        <?php echo lang('sex', 'sex'); ?>
          <select name="gender" placeholder="Gender" class="form-control">
            <?php
            foreach ($sex as $key=>$val) {
              ?>
              <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
            <?php
            }
            ?>
          </select>
        </div>
        <div class="form-group col-md-4">
        <?php echo lang('dob', 'dob'); ?>
          <input type="date" name="DOB" placeholder="Date Of Birth" class="form-control" required>
        </div>

        <div class="form-group col-md-12">
        <?php echo lang('street', 'street'); ?>
          <input type="text" class="form-control" name="street" required>
        </div>

        <div class="form-group col-md-4">
        <?php echo lang('postal_code', 'postal_code'); ?>
          <input type="text" class="form-control" name="postal_code" required>
        </div>

        <div class="form-group col-md-4">
        <?php echo lang('city', 'city'); ?>
          <input type="text" class="form-control" name="city" required>
        </div>
        <div class="form-group col-md-4">
        <?php echo lang('state', 'state'); ?>
          <select name="state" placeholder="state" class="form-control" required>
            <?php
            foreach ($state as $row) {
              ?>
              <option value="<?php echo $row->option_id; ?>"><?php echo $row->title; ?></option>
            <?php
            }
            ?>
          </select>
        </div>

        <div class="form-group col-md-12">
        <?php echo lang('marital_status', 'city'); ?> : &nbsp;
          <?php
          foreach ($marital as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="marital_status" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>

        <div class="form-group col-md-6">
        <?php echo lang('prev_doctor', 'prev_doctor'); ?>
          <input type="text" class="form-control" name="prev_doctor" id="">
        </div>

        <div class="form-group col-md-6">
        <?php echo lang('date_physical_exam', 'date_physical_exam'); ?>
          <input type="date" name="date_physical_exam" class="form-control">
        </div>
      </fieldset>

      <fieldset>
      <legend><?php echo $this->lang->line('leg_health_history'); ?></legend>

        <div class="form-group col-md-12">
        <?php echo lang('chilhood_illness', 'chilhood_illness'); ?> &nbsp;<br>
          <?php
          foreach ($childhood as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="chilhood_illness" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>

        <div class="form-group col-md-12">
        <?php echo lang('immunization', 'immunization'); ?>
          <?php
          foreach ($immunization as $key=>$val) {
            ?>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="immunization[]" value="<?php echo $key; ?>"><?php echo $val; ?></label>
            </div>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('medical_problems', 'medical_problems'); ?>
          <textarea class="form-control" rows="3" id="medical_problems" name="medical_problems"></textarea>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('surgeries', 'surgeries'); ?>
        </div>
        <div class="form-group col-md-4">
          <input type="text" class="form-control" name="surgeries_year" id="surgeries_year" placeholder="<?php echo $this->lang->line('year'); ?>">
        </div>

        <div class="form-group col-md-4">
          <input type="text" class="form-control" name="surgeries_reason" id="" placeholder="<?php echo $this->lang->line('reason'); ?>">
        </div>
        <div class="form-group col-md-4">
          <input type="text" class="form-control" name="surgeries_hospital" id="" placeholder="<?php echo $this->lang->line('hospital'); ?>">
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('other_hospital', 'other_hospital'); ?>
        </div>
        <div class="form-group col-md-4">
          <input type="text" class="form-control" name="other_hospital_year" id="other_hospital" placeholder="<?php echo $this->lang->line('year'); ?>">
        </div>

        <div class="form-group col-md-4">
          <input type="text" class="form-control" name="other_hospital_reason" id="" placeholder="<?php echo $this->lang->line('reason'); ?>">
        </div>
        <div class="form-group col-md-4">
          <input type="text" class="form-control" name="other_hospital_hospital" id="" placeholder="<?php echo $this->lang->line('hospital'); ?>">
        </div>

        <div class="form-group col-md-12">
        <?php echo lang('blood_transfusion', 'blood_transfusion'); ?>
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="blood_transfusion" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>

        </div>

        <div class="form-group col-md-12">
        <?php echo lang('prescribed_list', 'prescribed_list'); ?>
        </div>
        <div class="form-group col-md-4">
          <input type="text" class="form-control" name="prescribed_list_drug_name" id="first_name" placeholder="<?php echo $this->lang->line('name_the_drug'); ?>">
        </div>

        <div class="form-group col-md-4">
          <input type="text" class="form-control" name="prescribed_list_strength" id="" placeholder="<?php echo $this->lang->line('strength'); ?>">
        </div>
        <div class="form-group col-md-4">
          <input type="text" class="form-control" name="prescribed_list_frequency_taken" id="" placeholder="<?php echo $this->lang->line('frequency_taken'); ?>">
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('allergies', 'allergies'); ?>
        </div>
        <div class="form-group col-md-4">
          <input type="text" class="form-control" name="allergies_drug_name" id="allergies" placeholder="<?php echo $this->lang->line('name_the_drug'); ?>">
        </div>

        <div class="form-group col-md-4">
          <input type="text" class="form-control" name="allergies_reaction" id="" placeholder="<?php echo $this->lang->line('reaction'); ?>">
        </div>
      </fieldset>

      <fieldset>
      <legend>
        <?php echo $this->lang->line('leg_health_habbit'); ?>
      </legend>
      <center><?php echo $this->lang->line('leg_health_habbit_desc'); ?></center>

        <div class="form-group col-md-12 title">
        <?php echo lang('sec_exercise', 'sec_exercise'); ?>
        </div>
        <div class="form-group col-md-12">
          <?php
          foreach ($exercise as $key=>$val) {
            ?>
            <div class="radio">
              <label>
                <input type="radio" name="exercise" value="<?php echo $key; ?>"><?php echo $val; ?></label>
            </div>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12 title">
        <?php echo lang('sec_diet', 'sec_diet'); ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('medical_diet', 'medical_diet'); ?>
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="diet" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('prescribed_medical_diet', 'prescribed_medical_diet'); ?>
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="medical_diet" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('prescribed_medical_diet2', 'prescribed_medical_diet2'); ?>
          <input type="text" placeholder="" class="form-control" name="eat_average">
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('salt_intake', 'salt_intake'); ?> :&nbsp;
          <?php
          foreach ($rank_intake as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="salt_intake" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('fat_intake', 'fat_intake'); ?> :&nbsp;
          <?php
          foreach ($rank_intake as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="fat_intake" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('sec_caffeine', 'sec_caffeine'); ?> &nbsp;
          <?php
          foreach ($caffein as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="caffeine" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('cups_perday', 'cups_perday'); ?> &nbsp;
          <input type="text" placeholder="" class="form-control" name="cups_perday">
        </div>

        <div class="form-group col-md-12 title">
        <?php echo lang('sec_alcohol', 'sec_alcohol'); ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('drink_alcohol', 'drink_alcohol'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="drink_alcohol" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-6">
        <?php echo lang('alcohol_kind', 'alcohol_kind'); ?> &nbsp;
          <input type="text" placeholder="" class="form-control" name="alcohol_kind">
        </div>
        <div class="form-group col-md-6">
        <?php echo lang('drink_perweek', 'drink_perweek'); ?> &nbsp;
          <input type="text" placeholder="" class="form-control" name="drink_perweek">
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('drink_amount', 'drink_amount'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="drink_amount" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('consider_stop', 'consider_stop'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="consider_stop" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('blackout', 'blackout'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="blackout" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('binge_drink', 'binge_drink'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="binge_drink" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('after_drink', 'after_drink'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="after_drink" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>

        <div class="form-group col-md-12 title">
        <?php echo lang('sec_tobacco', 'sec_tobacco'); ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('use_tobacco', 'use_tobacco'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="use_tobacco" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
          <?php
          foreach ($tobaccos as $key=>$val) {
            ?>
            <div class="radio">
              <label>
                <input type="radio" name="which_tobacco" value="<?php echo $key; ?>"><?php echo $val; ?></label>
            </div>
          <?php
          }
          ?>
        </div>

        <div class="form-group col-md-12 title">
        <?php echo lang('sec_drugs', 'sec_drugs'); ?>
        </div>

        <div class="form-group col-md-12">
        <?php echo lang('street_drug', 'street_drug'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="street_drug" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('drug_needle', 'drug_needle'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="drug_needle" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12 title">
        <?php echo lang('sec_sex', 'sec_sex'); ?>
        </div>

        <div class="form-group col-md-12">
        <?php echo lang('active_sex', 'active_sex'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="active_sex" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('pregnancy', 'pregnancy'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="pregnancy" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('contraceptive', 'contraceptive'); ?> &nbsp;
          <input type="text" placeholder="" class="form-control" name="contraceptive">
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('intercourse', 'intercourse'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="intercourse" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('illness_related', 'illness_related'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="illness_related" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12 title">
        <?php echo lang('sec_personal_safety', 'sec_personal_safety'); ?> &nbsp;
        </div>

        <div class="form-group col-md-12">
        <?php echo lang('live_alone', 'live_alone'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="live_alone" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('frequent_fall', 'frequent_fall'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="frequent_fall" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('vision_loss', 'vision_loss'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="vision_loss" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('living_will', 'living_will'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="living_will" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('information', 'information'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="information" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('mental_abuse', 'mental_abuse'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="mental_abuse" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
      </fieldset>

      <fieldset>
      <legend><?php echo $this->lang->line('leg_family'); ?></legend>

        <div class="form-group col-md-12 title">
        <?php echo lang('sec_father', 'sec_father'); ?>
        </div>

        <div class="form-group col-md-4">
          <input type="number" class="form-control" name="father_age" id="first_name" placeholder="<?php echo $this->lang->line('age'); ?>">
        </div>

        <div class="form-group col-md-8">
          <input type="text" class="form-control" name="father_problem" id="" placeholder="<?php echo $this->lang->line('significant'); ?>">
        </div>

        <div class="form-group col-md-12 title">
        <?php echo lang('sec_mother', 'sec_mother'); ?> 
        </div>

        <div class="form-group col-md-4">
          <input type="number" class="form-control" name="" id="mother_age" placeholder="<?php echo $this->lang->line('age'); ?>">
        </div>

        <div class="form-group col-md-8">
          <input type="text" class="form-control" name="mother_problem" id="" placeholder="<?php echo $this->lang->line('significant'); ?>">
        </div>
        <div class="form-group col-md-12 title">
        <?php echo lang('sec_sibling', 'sec_sibling'); ?>
        </div>

        <div class="form-group col-md-4">
        <?php
          foreach ($sex as $key=>$val) {
            ?>
            <div class="radio">
              <label>
                <input type="radio" name="sibling_sex" value="<?php echo $key; ?>"><?php echo $val; ?></label>
            </div>
          <?php
          }
          ?>
        </div>

        <div class="form-group col-md-8">
          <input type="text" class="form-control" name="sibling_problem" id="" placeholder="<?php echo $this->lang->line('significant'); ?>">
        </div>

        <div class="form-group col-md-12 title">
        <?php echo lang('sec_children', 'sec_children'); ?>
        </div>

        <div class="form-group col-md-4">
          <?php
          foreach ($sex as $key=>$val) {
            ?>
            <div class="radio">
              <label>
                <input type="radio" name="children_sex" value="<?php echo $key; ?>"><?php echo $val; ?></label>
            </div>
          <?php
          }
          ?>
        </div>

        <div class="form-group col-md-8">
          <input type="text" class="form-control" name="children_problem" id="" placeholder="<?php echo $this->lang->line('significant'); ?>">
        </div>

        <div class="form-group col-md-12 title">
        <?php echo lang('sec_grandmother_mat', 'sec_grandmother_mat'); ?>&nbsp;<span>Maternal</span>
        </div>

        <div class="form-group col-md-4">
          <input type="number" class="form-control" name="grandmother_age" id="first_name" placeholder="<?php echo $this->lang->line('age'); ?>">
        </div>

        <div class="form-group col-md-8">
          <input type="text" class="form-control" name="grandmother_problem" id="" placeholder="<?php echo $this->lang->line('significant'); ?>">
        </div>
        <div class="form-group col-md-12 title">
        <?php echo lang('sec_grandfather_mat', 'sec_grandfather_mat'); ?>&nbsp;<span>Maternal</span>
        </div>

        <div class="form-group col-md-4">
          <input type="number" class="form-control" name="grandfather_age" id="grandfather" placeholder="<?php echo $this->lang->line('age'); ?>">
        </div>

        <div class="form-group col-md-8">
          <input type="text" class="form-control" name="grandfather_problem" id="" placeholder="<?php echo $this->lang->line('significant'); ?>">
        </div>
        <div class="form-group col-md-12 title">
        <?php echo lang('sec_grandfather_pat', 'sec_grandfather_pat'); ?>&nbsp;<span>Paternal</span>
        </div>

        <div class="form-group col-md-4">
          <input type="number" class="form-control" name="grandmother_pat_age" id="grandmother_pat_age" placeholder="<?php echo $this->lang->line('age'); ?>">
        </div>

        <div class="form-group col-md-8">
          <input type="text" class="form-control" name="grandmother_pat_problem" id="" placeholder="<?php echo $this->lang->line('significant'); ?>">
        </div>
        <div class="form-group col-md-12 title">
        <?php echo lang('sec_grandfather_pat', 'sec_grandfather_pat'); ?>&nbsp;<span>Paternal</span>
        </div>

        <div class="form-group col-md-4">
          <input type="number" class="form-control" name="grandfather_pat_age" id="first_name" placeholder="<?php echo $this->lang->line('age'); ?>">
        </div>

        <div class="form-group col-md-8">
          <input type="text" class="form-control" name="grandfather_pat_problem" id="" placeholder="<?php echo $this->lang->line('significant'); ?>">
        </div>
      </fieldset>

      <fieldset>
      <legend><?php echo $this->lang->line('leg_mental_health'); ?></legend>

        <div class="form-group col-md-12">
        <?php echo lang('stress_problem', 'stress_problem'); ?>
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="stress_problem" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('depressed', 'depressed'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="depressed" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('panic_stressed', 'panic_stressed'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="panic_stressed" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('eat_problem', 'eat_problem'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="eat_problem" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('cry_frequent', 'cry_frequent'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="cry_frequent" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('suicide', 'suicide'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="suicide" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('self_hurting', 'self_hurting'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="self_hurting" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>

        <div class="form-group col-md-12">
        <?php echo lang('trouble_sleep', 'trouble_sleep'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="trouble_sleep" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('counselor', 'counselor'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="counselor" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
      </fieldset>

      <fieldset>
      <legend><?php echo $this->lang->line('leg_women_only'); ?></legend>
        <div class="form-group col-md-6">
        <?php echo lang('age_menstruation', 'age_menstruation'); ?> &nbsp;
          <input type="text" class="form-control" name="age_menstruation" id="age_menstruation">
        </div>

        <div class="form-group col-md-6">
        <?php echo lang('last_date_mens', 'last_date_mens'); ?>
          <input type="date" class="form-control" name="last_date_mens" id="" placeholder="DD/MM/YYYY">
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('periode_mens', 'periode_mens'); ?>
          <input type="text" class="form-control" name="periode_mens" id="" placeholder="<?php echo $this->lang->line('days'); ?>">
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('heavy_periode', 'heavy_periode'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="heavy_periode" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>

        <div class="form-group col-md-6">
        <?php echo lang('number_pregnancy', 'number_pregnancy'); ?>
          <input type="text" class="form-control" name="number_pregnancy" id="number_pregnancy">
        </div>

        <div class="form-group col-md-6">
        <?php echo lang('number_of_live', 'number_of_live'); ?>
          <input type="text" class="form-control" name="number_of_live" id="number_of_live">
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('are_you_pregnant', 'are_you_pregnant'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="are_you_pregnant" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('had_dc', 'had_dc'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="had_dc" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('urinary_tract', 'urinary_tract'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="urinary_tract" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('urine_blood', 'urine_blood'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="urine_blood" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('urinary_control', 'urinary_control'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="urinary_control" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('hot_flash', 'hot_flash'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="hot_flash" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('menstrual_tension', 'menstrual_tension'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="menstrual_tension" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('recent_breast', 'recent_breast'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="recent_breast" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>

        <div class="form-group col-md-6">
        <?php echo lang('rectal_exam', 'rectal_exam'); ?> &nbsp;
        </div>
        <div class="form-group col-md-6">
          <input type="date" class="form-control" name="rectal_exam" id="rectal_exam" placeholder="DD/MM/YYYY">
        </div>

      </fieldset>

      <fieldset>
      <legend><?php echo $this->lang->line('leg_men_only'); ?></legend>

        <div class="form-group col-md-12">
        <?php echo lang('urine_at_night', 'urine_at_night'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="urine_at_night" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-6">
        <?php echo lang('of_times_exam', 'of_times_exam'); ?> &nbsp;
          <input type="text" class="form-control" name="of_times_exam" id="" placeholder="">
        </div>

        <div class="form-group col-md-12">
        <?php echo lang('feel_pain', 'feel_pain'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="feel_pain" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('any_blood_in_urine', 'any_blood_in_urine'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="any_blood_in_urine" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('feel_burning_penis', 'feel_burning_penis'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="feel_burning_penis" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('urination_decrease', 'urination_decrease'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="urination_decrease" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('infection_12', 'infection_12'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="infection_12" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('bladder_problem', 'bladder_problem'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="bladder_problem" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('ejaculation', 'ejaculation'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="ejaculation" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('testicle_pain', 'testicle_pain'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="testicle_pain" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
        <div class="form-group col-md-12">
        <?php echo lang('last_prostate', 'last_prostate'); ?> &nbsp;
          <?php
          foreach ($yesno as $key=>$val) {
            ?>
            <label class="radio-inline">
              <input type="radio" name="last_prostate" value="<?php echo $key; ?>"><?php echo $val; ?></label>
          <?php
          }
          ?>
        </div>
      </fieldset>

      <fieldset>
      <legend><?php echo $this->lang->line('leg_other_problem'); ?></legend>
      <?php echo lang('other_problem', 'other_problem'); ?> &nbsp;
        <div class="form-group col-md-12">

          <?php
          foreach ($other_problems as $key=>$val) {
            ?>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="other_problem[]" value="<?php echo $key; ?>"><?php echo $val; ?></label>
            </div>
          <?php
          }
          ?>
        </div>
      </fieldset>

      <div class="form-group">
        <div class="col-md-12">
          <div class="radio">
            <label>
              <input type="radio" value="" id=""> <?php echo $this->lang->line('accept'); ?>
            </label>
          </div>
        </div>
      </div>

      <div class="form-group" style="margin-top: 25px">
        <div class="col-md-12" style="margin-bottom: 50px">
          <button type="submit" class="btn btn-primary" id="btn">
            <?php echo $this->lang->line('submit'); ?>
          </button>
        </div>
      </div>

      </form>
    </div>

  </div>
</div>