<?php
class LanguageLoader
{
    function initialize() {
        $ci =& get_instance();
        $ci->load->helper('language');
        $siteLang = $ci->session->userdata('site_lang');
        if ($siteLang) {
            $ci->lang->load('intake_form',$siteLang);
        } else {
            $ci->session->set_userdata('site_lang', 'spanish');
            $ci->lang->load('intake_form','spanish');
        }
    }
}